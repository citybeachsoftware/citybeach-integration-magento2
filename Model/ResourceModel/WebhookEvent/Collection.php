<?php

namespace CityBeach\Integration\Model\ResourceModel\WebhookEvent;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use CityBeach\Integration\Model\ResourceModel\WebhookEvent;

/**
 * Class Collection
 * @package CityBeach\Integration\Model\ResourceModel\WebhookEvent
 */
class Collection extends AbstractCollection
{
    /**
     * ID Field Name
     *
     * @var string
     */
    protected $_idFieldName = 'webhook_event_id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'citybeach_webhook_event';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'history_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\CityBeach\Integration\Model\WebhookEvent::class, WebhookEvent::class);
    }
}
