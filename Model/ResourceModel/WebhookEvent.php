<?php

namespace CityBeach\Integration\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Webhook Event class.
 * @package CityBeach\Integration\Model\ResourceModel;
 */
class WebhookEvent extends AbstractDb {
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('citybeach_webhook_event', 'webhook_event_id');
    }
}
