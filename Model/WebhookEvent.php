<?php

namespace CityBeach\Integration\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Webhook Event class.
 * @package CityBeach\Integration\Model;
 */
class WebhookEvent extends AbstractModel
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'citybeach_webhook_event';

    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = 'citybeach_webhook_event';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'citybeach_webhook_event';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\WebhookEvent::class);
    }
}
