<?php

namespace CityBeach\Integration\Cron;

use Exception;
use CityBeach\Integration\Model\WebhookEventFactory;
use CityBeach\Integration\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ApplyRule
 * @package Mageplaza\Webhook\Cron
 */
class EventForwarder
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var WebhookEventFactory
     */
    protected $factory;

    /**
     * @var ScopeConfigInterface
     */
    private $scope;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * CronSchedule constructor.
     *
     * @param WebhookEventFactory $webhookEventFactory
     * @param Date $helper
     * @param ScopeConfigInterface $scope
     * @param LoggerInterface $logger
     */
    public function __construct(WebhookEventFactory $webhookEventFactory, Data $helper, ScopeConfigInterface $scope, LoggerInterface $logger) {
        $this->factory = $webhookEventFactory;
        $this->helper = $helper;
        $this->scope = $scope;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $verbose = false;
        if ($verbose) $this->logger->info('CityBeach Event Forwarder');

        $enabled = $this->scope->isSetFlag('citybeach/webhook/enabled');

        if ( $enabled ) {
            if ($verbose) $this->logger->info('CityBeach Event Forwarder is enabled');
            $hostnames = $this->scope->getValue('citybeach/webhook/hostname');
            $codes = $this->scope->getValue('citybeach/webhook/code');
            $keys = $this->scope->getValue('citybeach/webhook/key');

            $limit = intval($this->scope->getValue('citybeach/webhook/limit'));
            $limit = $limit < 1 ? 1 : $limit;

            $hostnames = explode(',', trim($hostnames));
            $codes = explode(',', trim($codes));
            $keys = explode(',', trim($keys));
            if ( count($hostnames) != count($codes) ||  count($hostnames) != count($keys) ) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Citybeach webhook configuration is invalid')
                );
            }

            $webhookEventCollection = $this->factory->create()->getCollection()
                ->addFieldToFilter('entity_type', 'product')
                ->addFieldToFilter('event_type', 'update')
                ->addFieldToFilter('tries', ['lt' => 3])
                ->addFieldToFilter('response', ['neq' => 200])
                ->setOrder('updated_at', 'ASC');
            $webhookEventCollection->getSelect()->limit($limit);
            if ($verbose) $this->logger->info('CityBeach Event Forwarder found: ' . $webhookEventCollection->getSize());

            foreach ($webhookEventCollection->getItems() as $item) {
                try {
                    foreach ($hostnames as $targetIndex => $hostname) {
                        $this->helper->webhookEventSend($hostname, $codes[$targetIndex], $keys[$targetIndex], $item->getData('entity_type'), $item->getData('event_type'), $item->getData('entity_id'));
                    }
                    $result = true;
                } catch (Exception $exception) {
                    $this->logger->error("CityBeach webhook send issue: " . $exception->getLogMessage());
                    $result = false;
                }

                // update the record with the attempt result.
                $item->setData('tries',$item->getData('tries') +  1);
                $item->setData('response', $result ? 200 : -1);
                $item->setData('updated_at', new \Zend_Db_Expr('NOW()'));
                $item->save();
                if ($verbose)  $this->logger->info('CityBeach Event Forwarder response: ' . $result. ' for ' . $item->getData('entity_id'));
            }
        }
    }
}
