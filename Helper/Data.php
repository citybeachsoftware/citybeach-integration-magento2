<?php

namespace CityBeach\Integration\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use PHPUnit\Framework\Exception;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CurlFactory
     */
    protected $curlFactory;


    public function __construct(Context $context, StoreManagerInterface $storeManager, CurlFactory $curlFactory)
    {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->curlFactory = $curlFactory;
    }

    public function getStoreUrl()
    {
        $storeId = $this->storeManager->getDefaultStoreView()->getStoreId();
        $url = $this->storeManager->getStore($storeId)->getUrl();
        return $url;
    }

    public function webhookEventSend($hostname, $code, $key, $resource, $event, $id)
    {
        if ( $resource != 'product' || $event != 'update' ) {
            throw new Exception("Status () : unexpected operation, only product updates are currently supported");
        }

        try {
            $url = 'https://' . $hostname . '/magento2/webhook?resource=product&event=update' .
                '&retailerCode=' . $code .
                '&key=' . $key .
                '&productId=' . $id;

            $body =  json_encode(['body' => 'nil']);
            return $this->sendHttpRequest($url, 'POST', 'application/json', $body);
        } catch (Exception $exception) {
            throw new Exception("Host:" . $hostname . " Code: " . $code . " Message: " . $exception->getLogMessage());
        }
    }

    /**
     * @param $url
     * @param $method
     * @param $contentType
     * @param $body
     *
     * @return void
     */
    public function sendHttpRequest($url, $method, $contentType, $body)
    {
        $headersConfig = [];
        if ($contentType) {
            $headersConfig[] = 'Content-Type: ' . $contentType;
        }

        $curl = $this->curlFactory->create();
        try {
            $curl->write($method, $url, '1.1', $headersConfig, $body);

            $resultCurl = $curl->read();
            if (!empty($resultCurl)) {
                $statusCode = $this->extractCode($resultCurl);
                if (! isset($statusCode) || $statusCode != 200) {
                    $statusMessage = $this->extractMessage($resultCurl);
                    throw new Exception("Status (" . $statusCode . ") : " . $statusMessage);
                }
            }
            else {
                throw new Exception("Status () : no result");
            }
        }
        finally {
            $curl->close();
        }
    }

    /**
     * Extract the response code from a response string
     *
     * @param string $response_str
     * @return int
     */
    private function extractCode($response_str)
    {
        preg_match("|^HTTP/[\d\.x]+ (\d+)|", $response_str, $m);

        if (isset($m[1])) {
            return (int) $m[1];
        } else {
            return false;
        }
    }

    /**
     * Extract the HTTP message from a response
     *
     * @param string $response_str
     * @return string
     */
    private function extractMessage($response_str)
    {
        preg_match("|^HTTP/[\d\.x]+ \d+ ([^\r\n]+)|", $response_str, $m);

        if (isset($m[1])) {
            return $m[1];
        } else {
            return false;
        }
    }

}
