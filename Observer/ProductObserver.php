<?php

namespace CityBeach\Integration\Observer;

use Exception;
use Magento\CatalogImportExport\Model\Import\Product;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use CityBeach\Integration\Model\WebhookEventFactory;
use \Magento\Framework\ObjectManagerInterface;

/**
 * Class ProductObserver
 * @package CityBeach\Integration\Observer
 */
class ProductObserver implements ObserverInterface
{
    /*
     * @var ObjectManagerInterface $objectManager
    */
    private $objectManager;

    /**
     * @var WebhookEventFactory
     */
    private $factory;

    /**
     * @var ScopeConfigInterface
     */
    private $scope;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param WebhookEventFactory $factory
     * @param ObjectManagerInterface $objectManager
     * @param ScopeConfigInterface $scope
     * @param LoggerInterface $logger
     */
    public function __construct(WebhookEventFactory $factory, ObjectManagerInterface $objectManager, ScopeConfigInterface $scope, LoggerInterface $logger) {
        $this->factory = $factory;
        $this->objectManager = $objectManager;
        $this->scope = $scope;
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     *
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        $verbose = false;
        if ($verbose) $this->logger->info('CityBeach Product Observer');
        $enabled = $this->scope->isSetFlag('citybeach/webhook/enabled');
        if ( $enabled ) {
            if ($verbose) $this->logger->info('CityBeach Product Observer is enabled');
            $event = $observer->getEvent();
            $eventName = $event->getName();
            $item = $observer->getDataObject();

            if ($verbose) $this->logger->info('CityBeach Product Observer event details',['name'=>$eventName, 'item'=>$item]);

            switch ( $eventName ) {
                case 'catalog_product_import_bunch_save_after':
                    $bunch = $event->getBunch();
                    $adapter = $event->getAdapter();
                    $entityIds = [];
                    $eventType = 'update';
                    foreach ($bunch as $rowData) {
                        $productData = $adapter->getNewSku($rowData[Product::COL_SKU]);
                        if (isset($productData['entity_id'])) {
                            array_push($entityIds, $productData['entity_id']);
                        }
                    }
                    break;
                case 'catalog_product_attribute_update_before':
                    $entityIds = $event->getData('product_ids');
                    $eventType = 'update';
                    break;
                case 'catalog_product_save_after':
                    $entityIds = [$item->getId()];
                    $eventType = 'update';
                    break;
                case 'catalog_product_delete_before':
                    $entityIds = [$item->getId()];
                    $eventType = 'delete';
                    break;
                default:
                    return;
            }

            if ($verbose) $this->logger->info('CityBeach Product Observer details', ['eventType' => $eventType, 'entityIds' => $entityIds]);

            foreach ( $entityIds as $entityId ) {
                $webhookEventCollection = $this->factory->create()->getCollection()
                    ->addFieldToFilter('entity_type', 'product')
                    ->addFieldToFilter('event_type', 'update')
                    ->addFieldToFilter('entity_id', $entityId)
                    ->setOrder('updated_at', 'ASC');

                if ($webhookEventCollection->getSize() > 0) {
                    $cleanup = false;
                    foreach ($webhookEventCollection->getItems() as $item) {
                        if ($cleanup) {
                            // delete other records
                            $item->delete();
                        } else {
                            // update first existing records
                            $item->setData('tries', 0);
                            $item->setData('response', 0);
                            $item->setData('updated_at', new \Zend_Db_Expr('NOW()'));
                            $item->setData('event_type', $eventType);
                            $item->save();
                            $cleanup = true;
                            if ($verbose) $this->logger->info("updated webhook event for: " . $entityId);
                        }
                    }
                } else {
                    // create a new record
                    $webhookEvent = $this->factory->create();
                    $data = [
                        'entity_type' => 'product',
                        'event_type' => $eventType,
                        'entity_id' => $entityId,
                        'tries' => 0,
                        'response' => 0,
                        'created_at' => new \Zend_Db_Expr('NOW()'),
                        'updated_at' => new \Zend_Db_Expr('NOW()'),
                    ];
                    try {
                        $webhookEvent->addData($data);
                        $webhookEvent->save();
                        if ($verbose) $this->logger->info("updated webhook event for: " . $entityId);
                    } catch (Exception $exception) {
                        $this->messageManager->addError($exception->getMessage());
                    }
                }
            }
        }
    }
}
