<?php

namespace CityBeach\Integration\Observer;

use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Magento\Authorization\Model\UserContextInterface;
use Magento\Integration\Api\IntegrationServiceInterface;

/**
 * Class PaymentMethodAvailable
 * @package CityBeach\Integration\Observer
 */
class PaymentMethodAvailable implements ObserverInterface
{
    /**
     * @var UserContextInterface
     */
    private $userContext;

    /**
     * @var IntegrationServiceInterface
     */
    private $integrationService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param UserContextInterface $userContext
     * @param IntegrationServiceInterface $integrationService
     * @param User $userResource
     * @param LoggerInterface $logger
     */
    public function __construct(UserContextInterface $userContext, IntegrationServiceInterface $integrationService, LoggerInterface $logger) {
        $this->userContext = $userContext;
        $this->integrationService = $integrationService;
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     *
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        $verbose = false;
        if ($verbose) $this->logger->info('CityBeach PaymentMethodAvailable Observer');

        if( in_array($observer->getEvent()->getMethodInstance()->getCode(), ['citybeachpayment', 'omnivorepayment', 'traderunnerpayment', 'catchfeederpayment'])){
            $userId = $this->userContext->getUserId();
            $userType = $this->userContext->getUserType();
            if ($verbose) $this->logger->info('userId: ' . $userId . ' userType: ' . $userType);
            if ($userId && $userType == UserContextInterface::USER_TYPE_INTEGRATION) {
                $integration = $this->integrationService->get($userId);
                if ($verbose) $this->logger->info('integration name: ' . $integration->getName());
                if ( in_array($integration->getName(), ['CityBeach Integration', 'CityBeach', 'CityBeach Override']) ) {
                    if ($verbose) $this->logger->info('integration accepted');
                    return;
                }
            }

            $checkResult = $observer->getEvent()->getResult();
            $checkResult->setData('is_available', false);
            if ($verbose) $this->logger->info('making unavailable: ' . $observer->getEvent()->getMethodInstance()->getCode());
        }
    }
}
