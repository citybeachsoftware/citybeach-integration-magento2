<?php

namespace CityBeach\Integration\Api;

interface CityBeachInterface
{
    /**
     * Query magento for system and debugging information.
     *
     * @api
     * @return mixed
     */
    public function execute();

    /**
     * Query magento for system available inventory sources.
     *
     * @api
     * @return mixed
     */
    public function getSourceDetails();

    /**
     * Query the webhooks details.
     * @return mixed webhooks details
     */
    public function getWebhook();

    /**
     * Update the webhooks details.
     *
     * @param mixed details
     * @return bool
     */
    public function setWebhook($details);

}
